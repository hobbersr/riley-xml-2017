<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:math="http://www.w3.org/2005/xpath-functions/math" xmlns:array="http://www.w3.org/2005/xpath-functions/array" xmlns:map="http://www.w3.org/2005/xpath-functions/map" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:err="http://www.w3.org/2005/xqt-errors" exclude-result-prefixes="array fn map math xhtml xs err" version="3.0">
	<xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>
<!-- Good work here Riley.  This file wasn't well formed when I opened it.  It was missing
the closing xsl:for-each element I've added and commented out below.  Once I added that in
the file was fine and output the XML data well... there were some issues with formatting (customer info all on 1 line)
but otherwise it was good.
8/10
-->
	<xsl:template match="/" name="xsl:initial-template">
		<html>
			<head>
				<title>Customer Info:</title>
			</head>
			<body>
			<h2>Customer Info:</h2><br/><br/>
			
			<xsl:for-each select="telephoneBill/customer">
			
			Phone Number: <xsl:value-of select="@phoneNumber"/>
			Name: <xsl:value-of select="name"/>
			Address: <xsl:value-of select="address"/>
			City: <xsl:value-of select="city"/>
			Province: <xsl:value-of select="province"/>
			<!--</xsl:for-each>-->
			<br/><br/>
			Details:<br/><br/>
			<table border="1">
				<tbody>
					<tr>
						<th>Number Called</th>
						<th>Date</th>
						<th>Call Duration</th>
						<th>Charge</th>
					</tr>
					<xsl:for-each select="telephoneBill/call">
						<tr>
							<td><xsl:value-of select="@number"/></td>
							<td><xsl:value-of select="@date"/></td>
							<td><xsl:value-of select="@durationInMinutes"/></td>
							<td><xsl:value-of select="@charge"/></td>
						</tr>
					</xsl:for-each>
				</tbody>
			</table>
			</body>
		</html>	
	</xsl:template>
</xsl:stylesheet>
